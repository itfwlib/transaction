<?php

class Transaction_model extends CI_MODEL{
	protected $_tables;
	protected $_columns;

	public function __construct(){
		$this->load->model('core/general_model');
	}
	public function saveTrans($post = array(),array $config){
		$result 		= $this->general_model->result();
		if(!isset($post['transaction_id'])){
			$result->code 		= 402;
			$result->info 		= 'Transaction ID Required!';
			return $result;
		}
		$this->_tables	= $config['tables'];
		$this->_columns	= $config['columns'];


		$checkDuplicate = $this->checkDuplicate($post['transaction_id']);
		if($checkDuplicate->code != 0){
			return $checkDuplicate;
		}
		try {
			// assign value
			$saveData 		= array();
			foreach ($this->_columns as $key => $value) {
				$saveData[$value] 		= 	isset($post[$value]) ? $post[$value] : null;
			}

			$insert 		= $this->db->insert($this->_tables['transaction'],$post);
			if(!$insert){
				$result->code 	= 501;
				$result->info 	= 'Failed executing query';
			}
		} catch (Exception $e) {
			$result->code 		= 502;
			$result->info 	= 'Failed executing query';
		}

		return $result;
	}

	public function checkDuplicate($transaction_id){
		$result 		= $this->general_model->result();
		$this->db->where($this->_columns['transaction_id'], $transaction_id);
		$this->db->from($this->_tables['transaction']);
		$data 	= 	$this->db->get();
		if($data->row()){
			$result->code 	= 401;
			$result->info 	= 'Transaction Duplicate!';
			$result->data 	= $data->row();
		}
		return $result;
	}

}