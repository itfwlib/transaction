<?php

class Config{
	public static function getConfig(){
		return array(
			'tables' 		=> array(
				'transaction' 		=> 'transaction'
			),
			'columns' 		=> array(
				'transaction_id' 	=> 'transaction_id',
				'transaction_date' 	=> 'transaction_date',
				'transaction_amount'=> 'transaction_amount',
				'create_dtm' 		=> 'create_dtm',
				'create_by'			=> 'create_by'
			)
		);
	}
}