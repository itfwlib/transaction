<?php 
class Transaction extends CI_CONTROLLER{
	protected $_config;

	public function __construct(){
		parent::__construct();
		if(!@include APPPATH.'config/modules/transaction.php' ){
			// Check if dir exists. if not create new
			if(!file_exists(APPPATH.'config/modules/')){
				mkdir(APPPATH.'config/modules/',755);
			}

			$copy 	= copy(APPPATH.'modules/transaction/config/transaction.php',APPPATH.'config/modules/transaction.php');


			include APPPATH.'config/modules/transaction.php';

			$this->load->model('Installation_model');
			$sql 		= file_get_contents(APPPATH.'modules/transaction/resource/db.sql');
			$executeQ 	= $this->Installation_model->executeQuery($sql);
		}
		$this->_config 		= Config::getConfig();
	}
	
	public function saveTrans(){
		$post 			= $this->input->post();
		$this->load->model('Transaction_model');
		$result 		= $this->Transaction_model->saveTrans($post,$this->_config);
		
		echo json_encode($result);
		return false;
	}
}